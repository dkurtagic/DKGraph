//
//  DKGraphMLParser.h
//  DKGraph
//
//  Created by Dino Kurtagić on 11/11/14.
//  Copyright (c) 2014 Dino Kurtagić. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DKGraph.h"
#import "DKGraphEdge.h"
#import "DKGraphNode.h"

#define ifElement(str) if ([_elementName isEqualToString:str])
#define elifElement(str) else if ([_elementName isEqualToString:str])

#define bindStr(obj) obj = _elementValue
#define bindNo(obj) obj = [NSNumber numberWithDouble:[_elementValue doubleValue]]
#define bindInt(obj) obj = [_elementValue intValue]
#define bindFloat(obj) obj = [_elementValue floatValue]
#define bindDouble(obj) obj = [_elementValue doubleValue]

#define addToArray(array) [array addObject:_elementValue]

@interface DKGraphMLParser : NSXMLParser
{
    NSDictionary *_attributesDict;
    NSString *_elementValue;
    NSString *_elementName;
    NSMutableArray *_items;
    
    DKGraph *graph;
}

@property (readonly, nonatomic) NSError *error;
@property (readonly, nonatomic) NSArray *itemsArray;
@property (readonly, nonatomic) DKGraph *graphItem;
- (DKGraph*)parseGraphFromData:(NSData*)data; // GraphML representation
@end
