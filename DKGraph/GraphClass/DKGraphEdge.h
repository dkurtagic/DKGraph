//
//  DKGraphEdge.h
//  DKGraph
//
//  Created by Dino Kurtagić on 10/11/14.
//  Copyright (c) 2014 Dino Kurtagić. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DKGraphEdge : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *edgeID;
@property (nonatomic, strong) NSString *sourceNode;
@property (nonatomic, strong) NSString *targetNode;
@property (nonatomic, assign) double weight;
@end
