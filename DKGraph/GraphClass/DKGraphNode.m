//
//  DKGraphNode.m
//  DKGraph
//
//  Created by Dino Kurtagić on 10/11/14.
//  Copyright (c) 2014 Dino Kurtagić. All rights reserved.
//

#import "DKGraphNode.h"

@implementation DKGraphNode

-(instancetype)init
{
    self = [super init];
    if(self)
    {
        _edgeConnections = [NSMutableArray new];
    }
    return self;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"Node named: %@ with id:%@", self.name, self.nodeID];
}

@end
