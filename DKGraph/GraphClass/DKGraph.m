//
//  DKGraph.m
//  DKGraph
//
//  Created by Dino Kurtagić on 10/11/14.
//  Copyright (c) 2014 Dino Kurtagić. All rights reserved.
//

#import "DKGraph.h"

@implementation DKGraph
- (instancetype)init
{
    self = [super init];
    if(self)
    {
        _nodes = [NSMutableArray new];
        _edges = [NSMutableArray new];
    }
    return self;
}

- (void)printNodesDeg:(DKGraph*)graph
{
    for (DKGraphNode *node in graph.nodes)
    {
        NSPredicate *inPredicate = [NSPredicate predicateWithFormat:@"sourceNode == %@",node.nodeID];
        NSInteger inDeg = [graph.edges filteredArrayUsingPredicate:inPredicate].count;
        NSPredicate *outPredicate = [NSPredicate predicateWithFormat:@"targetNode == %@",node.nodeID];
        NSInteger outDeg = [graph.edges filteredArrayUsingPredicate:outPredicate].count;
        NSLog(@"Deg of node %@ is %ld", node.nodeID, inDeg+outDeg);
    }
}

- (void)breadthFirstSearch:(DKGraph*)graph
{
    NSLog(@"BreadthFirstSearch");
    NSMutableArray *s = [NSMutableArray new];
    NSMutableArray *Status = [NSMutableArray new];
    for(int i = 0; i < graph.nodes.count; i++)
    {
        [Status addObject:[NSNumber numberWithInteger:0]];
    }
    [s addObject:[graph.nodes objectAtIndex:0]];
    [Status addObject:[NSNumber numberWithInteger:1]];
    while (1)
    {
        if (s.count == 0 ) break;
        DKGraphNode *node = [s lastObject];
        [s removeLastObject];
        [Status removeLastObject];
        NSLog(@"node %@",node.nodeID);
        NSArray *neighborNodes = [self getNodeNeighbours:node inGraph:graph];
        for(DKGraphNode *neighbor in neighborNodes)
        {
            DKGraphNode *neighborNode = nil;
            NSInteger *indexOfNode = NULL;
            for(DKGraphNode *nodeInS in s)
            {
                if([nodeInS.nodeID isEqualToString:neighbor.nodeID])
                {
                    neighborNode = nodeInS;
                    indexOfNode = [s indexOfObject:nodeInS];
                }
            }
            if(!neighborNode)
            {
                [s addObject:neighbor];
                [Status addObject:[NSNumber numberWithInteger:1]];
//                NSNumber *statusOfNeighbor = [NSNumber numberWithInteger:[Status objectAtIndex:indexOfNode]];
//                if([statusOfNeighbor integerValue] == 0)
//                {
//                    [s addObject:neighborNode];
//                    [Status addObject:[NSNumber numberWithInteger:1]];
//                }
            }
//            else
//            {
//                [s addObject:neighbor];
//                [Status addObject:[NSNumber numberWithInteger:1]];
//            }
        }
    }
}

-(NSArray*)getNodeNeighbours:(DKGraphNode*)node inGraph:(DKGraph*)graph
{
    NSMutableArray *neighboursArray = [NSMutableArray new];
    for(DKGraphEdge *edge in node.edgeConnections)
    {
        for(DKGraphNode *node in graph.nodes)
        {
            if ([node.nodeID isEqualToString:edge.targetNode])
            {
                [neighboursArray addObject:node];
            }
        }
    }
    return [neighboursArray copy];
}
@end
