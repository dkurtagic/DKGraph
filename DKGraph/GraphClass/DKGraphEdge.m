//
//  DKGraphEdge.m
//  DKGraph
//
//  Created by Dino Kurtagić on 10/11/14.
//  Copyright (c) 2014 Dino Kurtagić. All rights reserved.
//

#import "DKGraphEdge.h"

@implementation DKGraphEdge

- (NSString *)description
{
    return [NSString stringWithFormat:@"Edge named: %@ with id:%@ between: %@ and %@ node and weight: %f", self.name,self.edgeID, self.sourceNode, self.targetNode, self.weight];
}

@end
