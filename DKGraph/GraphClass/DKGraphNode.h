//
//  DKGraphNode.h
//  DKGraph
//
//  Created by Dino Kurtagić on 10/11/14.
//  Copyright (c) 2014 Dino Kurtagić. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSDictionary+Extras.h"

@interface DKGraphNode : NSObject

@property (nonatomic, strong) NSString *nodeID;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSMutableArray *edgeConnections;
@property (nonatomic, strong) id data;

@end
